import { CookieService } from 'ngx-cookie-service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//

import { Routing, AppRoutingProviders } from './app.routes';

// models
import { AccountModel } from '@app/models/local/account/account.classes';

// directives
import { ActiveDirective } from '@app/directives/active.directive';
import { AutoHeightDirective } from '@app/directives/auto-height.directive';
import { FocusDirective } from '@app/directives/focus.directive';
import { ValidatorFieldDirective } from '@app/directives/validator-field.directive';
import { ValidatorFormDirective } from '@app/directives/validator-form.directive';
import { FileDropDirective } from '@app/directives/file-drop.directive';
import { FileSelectDirective } from '@app/directives/file-select.directive';
import { FileClickDirective } from '@app/directives/file-click.directive';

// services : core
import { InjectorService } from '@app/services/core/injector/injector.classes';
import { StorageCookieService } from '@app/services/core/storage/cookie.classes';
import { StorageLocalService } from '@app/services/core/storage/local.classes';
import { LanguageService } from '@app/services/core/language/language.classes';
import { MessagerService } from '@app/services/core/messenger/messenger.classes';
import { ValidatorService } from '@app/services/core/validator/validator.classes';

// services : interceptors
import { ResponseHttpInterceptor } from '@app/services/core/interceptors/interceptor.classes';

// services : guard
import { AuthenticationGuard } from '@app/services/guards/authentication/authentication.classes';
import { AuthenticationNotGuard } from '@app/services/guards/authentication-not/authentication-not.classes';

// services : api
import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';
import { ConnectAPI } from '@app/models/api/connect/connect.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';
import { OrderAPI } from '@app/models/api/order/order.classes';

// components
import { AppComponent } from '@app/app.component';
import { CreateAccountComponent } from '@app/components/create-account/create-account.component';
import { SignInComponent } from '@app/components/sign-in/sign-in.component';
import { ForgotPasswordComponent } from '@app/components/forgot-password/forgot-password.component';
import { ChangePasswordComponent } from '@app/components/change-password/change-password.component';

import { SharedUploaderComponent } from '@app/components/_shared/uploader/uploader.component';
import { SharedPaginationComponent } from '@app/components/_shared/pagination/pagination.component';

// components
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { ConfirmComponent } from '@app/components/confirm/confirm.component';

// components : message
import { MessageGlobalComponent } from '@app/components/message/global/global.component';
import { MessageFormComponent } from '@app/components/message/form/form.component';

// components : nav
import { NavTopComponent } from '@app/components/nav/top/top.component';
import { NavTopSignedInComponent } from '@app/components/nav/top-signed-in/top-signed-in.component';

// components : orders
import { SettingsOrdersListComponent } from '@app/components/settings/orders/list/list.component';
import { SettingsOrdersSingleComponent } from '@app/components/settings/orders/single/single.component';

// components : settings
import { SettingsSharedSidebarComponent } from '@app/components/settings/_shared/sidebar/sidebar.component';

import { SettingsProfileComponent } from '@app/components/settings/profile/profile.component';
import { SettingsProfileAvatarComponent } from '@app/components/settings/profile/avatar/avatar.component';
import { SettingsProfileHeadlineComponent } from '@app/components/settings/profile/headline/headline.component';
import { SettingsProfileStoryComponent } from '@app/components/settings/profile/story/story.component';

import { SettingsAccountComponent } from '@app/components/settings/account/account.component';
import { SettingsAccountUsernameComponent } from '@app/components/settings/account/username/username.component';
import { SettingsAccountEmailComponent } from '@app/components/settings/account/email/email.component';
import { SettingsAccountPasswordComponent } from '@app/components/settings/account/password/password.component';
import { SettingsAccountPasswordSetComponent } from '@app/components/settings/account/password-set/password-set.component';

import { SettingsConnectComponent } from '@app/components/settings/connect/connect.component';
import { SettingsConnectFacebookComponent } from '@app/components/settings/connect/facebook/facebook.component';
import { SettingsConnectGoogleComponent } from '@app/components/settings/connect/google/google.component';
import { SettingsConnectLinkedInComponent } from '@app/components/settings/connect/linkedin/linkedin.component';

import { SettingsPreferencesComponent } from '@app/components/settings/preferences/preferences.component';

// components : connect
import { ConnectFacebookComponent } from '@app/components/connect/facebook/facebook.component';
import { ConnectGoogleComponent } from '@app/components/connect/google/google.component';
import { ConnectLinkedInComponent } from '@app/components/connect/linkedin/linkedin.component';

@NgModule({
    declarations: [
        AppComponent, CreateAccountComponent, SignInComponent, ForgotPasswordComponent, ChangePasswordComponent,
        DashboardComponent, ConfirmComponent,

        SharedUploaderComponent, SharedPaginationComponent,

        ConnectFacebookComponent, ConnectGoogleComponent, ConnectLinkedInComponent,

        MessageGlobalComponent, MessageFormComponent,

        NavTopComponent, NavTopSignedInComponent,

        SettingsSharedSidebarComponent,

        SettingsOrdersListComponent, SettingsOrdersSingleComponent,

        SettingsProfileComponent,
        SettingsProfileAvatarComponent, SettingsProfileHeadlineComponent, SettingsProfileStoryComponent,

        SettingsAccountComponent,
        SettingsAccountUsernameComponent, SettingsAccountEmailComponent, SettingsAccountPasswordComponent, SettingsAccountPasswordSetComponent,

        SettingsConnectComponent,
        SettingsConnectFacebookComponent, SettingsConnectGoogleComponent, SettingsConnectLinkedInComponent,

        SettingsPreferencesComponent,

        ActiveDirective, AutoHeightDirective, FocusDirective, ValidatorFieldDirective, ValidatorFormDirective,
        FileDropDirective, FileSelectDirective, FileClickDirective
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        Routing,
        FormsModule, ReactiveFormsModule,
        FontAwesomeModule
    ],
    providers: [
        CookieService,

        {provide: HTTP_INTERCEPTORS, useClass: ResponseHttpInterceptor, multi: true},
        AppRoutingProviders,

        AccountModel,

        InjectorService, StorageCookieService, StorageLocalService, LanguageService, MessagerService, ValidatorService,

        AuthenticationGuard, AuthenticationNotGuard,

        AuthenticationAPI, ConnectAPI, AccountAPI, OrderAPI
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
    constructor(private injector: Injector) {
        InjectorService.Injector = this.injector;

        library.add(fas, far, fab);
    }
}
