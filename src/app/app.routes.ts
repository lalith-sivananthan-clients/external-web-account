import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// services : guards
import { AuthenticationNotGuard } from '@app/services/guards/authentication-not/authentication-not.classes';
import { AuthenticationGuard } from '@app/services/guards/authentication/authentication.classes';

// components
import { CreateAccountComponent } from '@app/components/create-account/create-account.component';
import { SignInComponent } from '@app/components/sign-in/sign-in.component';
import { ForgotPasswordComponent } from '@app/components/forgot-password/forgot-password.component';
import { ChangePasswordComponent } from '@app/components/change-password/change-password.component';

// components
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { ConfirmComponent } from '@app/components/confirm/confirm.component';

// components : orders
import { SettingsOrdersListComponent } from '@app/components/settings/orders/list/list.component';
import { SettingsOrdersSingleComponent } from '@app/components/settings/orders/single/single.component';

// components : settings
import { SettingsProfileComponent } from '@app/components/settings/profile/profile.component';
import { SettingsAccountComponent } from '@app/components/settings/account/account.component';
import { SettingsConnectComponent } from '@app/components/settings/connect/connect.component';
import { SettingsPreferencesComponent } from '@app/components/settings/preferences/preferences.component';

// components : connect
import { ConnectFacebookComponent } from '@app/components/connect/facebook/facebook.component';
import { ConnectGoogleComponent } from '@app/components/connect/google/google.component';
import { ConnectLinkedInComponent } from '@app/components/connect/linkedin/linkedin.component';

const appRoutes: Routes = [
    {
        path: 'create-account',
        component: CreateAccountComponent,
        canActivate: [AuthenticationNotGuard]
    },
    {
        path: 'sign-in',
        component: SignInComponent,
        canActivate: [AuthenticationNotGuard]
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        canActivate: [AuthenticationNotGuard]
    },
    {
        path: 'change-password/:token',
        component: ChangePasswordComponent,
        canActivate: [AuthenticationNotGuard]
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'confirm/:type/:token',
        component: ConfirmComponent,
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'connect',
        children: [
            {
                path: 'facebook',
                component: ConnectFacebookComponent
            },
            {
                path: 'google',
                component: ConnectGoogleComponent
            },
            {
                path: 'linkedin',
                component: ConnectLinkedInComponent
            }
        ]
    },
    {
        path: 'marketplace',
        children: [
            {
                path: 'orders',
                children: [
                    {
                        path: '',
                        component: SettingsOrdersListComponent,
                        canActivate: [AuthenticationGuard]
                    },
                    {
                        path: ':page',
                        component: SettingsOrdersListComponent,
                        canActivate: [AuthenticationGuard]
                    },
                ]
            },
            {
                path: 'order',
                children: [
                    {
                        path: ':orderID',
                        component: SettingsOrdersSingleComponent,
                        canActivate: [AuthenticationGuard]
                    },
                ]
            },
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'profile',
                component: SettingsProfileComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: 'account',
                component: SettingsAccountComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: 'connect',
                component: SettingsConnectComponent,
                canActivate: [AuthenticationGuard]
            },
            {
                path: 'preferences',
                component: SettingsPreferencesComponent,
                canActivate: [AuthenticationGuard]
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'create-account'
    }
];

export const AppRoutingProviders: any[] = [];
export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
