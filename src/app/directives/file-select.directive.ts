import { Directive, EventEmitter, ElementRef, Input, HostListener, Output } from '@angular/core';

@Directive({
	selector: '[sproutFileSelect]'
})
export class FileSelectDirective {

	// input

	@Input()
	sproutFileUploadMultiple: boolean;

	@Input()
	sproutFileSelect: File;

	@Input()
	sproutFilesSelect: Set<File>;

	// output

	@Output()
	sproutFileSelectChange: EventEmitter<File> = new EventEmitter<File>();

	@Output()
	sproutFilesSelectChange: EventEmitter<Set<File>> = new EventEmitter<Set<File>>();

	//

	protected element: ElementRef;

	public constructor(elementRef: ElementRef) {
		this.element = elementRef;
	}

	@HostListener('change')
	change() {
		if (this.sproutFileUploadMultiple === true) {
			const files: { [key: string]: File } = this.element.nativeElement.files;

			for (let key in files) {
				if (!isNaN(parseInt(key, 10))) {
					this.sproutFilesSelect.add(files[key]);
				}
			}

			this.sproutFilesSelectChange.emit(this.sproutFilesSelect);
		} else {
			this.sproutFileSelect = this.element.nativeElement.files;

			this.sproutFileSelectChange.emit(this.sproutFileSelect);
		}
	}

}
