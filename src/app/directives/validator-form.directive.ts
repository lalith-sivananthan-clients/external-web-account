import { Directive, EventEmitter, Input, Output, OnChanges } from '@angular/core';

import { FormGroup } from '@app/services/core/validator/form.classes';

@Directive({
	selector: '[sproutValidatorForm]'
})
export class ValidatorFormDirective implements OnChanges {

	@Input()
	sproutValidatorForm: FormGroup;

	@Output()
	sproutValidatorFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

	//

	constructor() {}

	ngOnChanges() {
		this.sproutValidatorForm.Validate();
		this.sproutValidatorFormChange.emit(this.sproutValidatorForm);
	}

}
