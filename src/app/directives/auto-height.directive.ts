import { Directive, HostListener, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Directive({
	selector: '[sproutAutoHeight]'
})
export class AutoHeightDirective {

	@Input()
	sproutAutoHeight: string;

	@Output()
	sproutAutoHeightChange: EventEmitter<any> = new EventEmitter<any>();

	//

	private el: ElementRef;

	constructor(_el: ElementRef) {
		this.el = _el;
	}

	stats() {
		console.log('Height Style:  ' + this.el.nativeElement.style.height);
		console.log('Height Scroll: ' + this.el.nativeElement.scrollHeight);
		console.log('Height Client: ' + this.el.nativeElement.clientHeight);
	}

	@HostListener('focus')
	focus() {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}

	@HostListener('change')
	change() {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}

	@HostListener('cut')
	cut() {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}

	@HostListener('paste')
	paste() {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}

	@HostListener('drop')
	drop() {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}

	@HostListener('keyup', ['$event'])
	keydown(event) {
		this.el.nativeElement.style.height = '0px';
		this.el.nativeElement.style.height = this.el.nativeElement.scrollHeight + 5 + 'px';
	}
}