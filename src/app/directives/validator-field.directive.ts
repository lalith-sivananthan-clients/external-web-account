import { Directive, EventEmitter, HostListener, Input, Output, OnChanges } from '@angular/core';

import { FormField } from '@app/services/core/validator/form.classes';

@Directive({
	selector: '[sproutValidatorField]'
})
export class ValidatorFieldDirective implements OnChanges {

	@Input()
	ngModel: string;

	@Input()
	sproutValidatorField: FormField;

	@Output()
	sproutValidatorFieldChange: EventEmitter<FormField> = new EventEmitter<FormField>();

	//

	value: string;

	constructor() {
		this.value = null;
	}

	@HostListener('focus')
	focus() {
		this.sproutValidatorField.Validate();
		this.sproutValidatorField.IsActive = true;
		this.sproutValidatorFieldChange.emit(this.sproutValidatorField);
	}

	@HostListener('blur')
	blur() {
		this.sproutValidatorField.Validate();
		this.sproutValidatorField.IsActive = false;
		this.sproutValidatorFieldChange.emit(this.sproutValidatorField);
	}

	ngOnChanges() {
		if (this.sproutValidatorField.Value === this.value) {
			return;
		}

		this.sproutValidatorField.Validate();

		this.value = this.sproutValidatorField.Value;
		this._checkIsEmpty();
	}

	// func : private

	_checkIsEmpty() {
		this.sproutValidatorField.IsEmpty = this.sproutValidatorField.Value.length <= 0;
		this._checkIsDirty();
	}

	_checkIsDirty() {
		if (this.sproutValidatorField.IsDirty === true) {
			this.sproutValidatorFieldChange.emit(this.sproutValidatorField);
			return;
		}

		this.sproutValidatorField.IsDirty = this.sproutValidatorField.Value !== this.sproutValidatorField.ValueOriginal;
		this.sproutValidatorFieldChange.emit(this.sproutValidatorField);
	}

}
