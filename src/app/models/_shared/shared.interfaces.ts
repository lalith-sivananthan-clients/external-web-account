import { ImageStatusEnum, UpdateEventTypeEnum } from '@app/models/_shared/shared.enums';

export interface IResponse {
	Success: boolean;
	Result: IResult;
}

export interface IResponseSearch {
	Success: boolean;
	Result: IResultSearch;
}

export interface IResult {
	Code: number;
	Data: any;
}

export interface IResultSearch {
	Code: number;
	PageCurrent: number;
	PageTotal: number;
	Data: any;
}

//
export interface IImageRef {
    MediaID: string;
    Status: ImageStatusEnum;
}

export interface IImage {
    MediaID: string;
    Status: ImageStatusEnum;
}

export interface IWhen {
	CreatedAt: string;
	UpdatedAt: string;
	DeletedAt: string;
}

//

export interface IUpdateEvent {
	Type: UpdateEventTypeEnum;
	Data: any;
}
