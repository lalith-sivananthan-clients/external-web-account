import { HttpHeaders } from '@angular/common/http';

export function GetHeader(): any {
    const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    });

    return {
        headers: headers,
        reportProgress: true
    };
}

export function GetHeaderWithUser(token: string): any {
    const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': token
    });

    return {
        headers: headers,
        reportProgress: true
    };
}

export function GetUploadHeaderWithUser(token: string, params?: any): any {
    const headers = new HttpHeaders({
        'Authorization': token
    });

    return {
        headers: headers,
        reportProgress: true
    };
}
