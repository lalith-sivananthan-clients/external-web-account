export enum ResultCodeEnum {
	Null = <number>0,
	Success = <number>1,
	Error = <number>2
}

//

export enum ImageStatusEnum {
    NotSet = <any>'notset',
    Set = <any>'set',
    Processing = <any>'processing'
}

//

export enum UpdateEventTypeEnum {
	Add = <any>'add',
	Update = <any>'update',
	Remove = <any>'remove'
}
