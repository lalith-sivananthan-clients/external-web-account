import { IImageRef, IWhen } from '@app/models/_shared/shared.interfaces';

export interface IProduct {
    ID: string;
    ShopID: string;
    Name: string;
    Slug: string;
    Description: string;
    SKU: string;
    Status: string;
    Group: string;
    Class: string;
    Method: string;
    Type: string;
    THC: number;
    CBD: number;
    Marketplace: IProductInventory;
    Brokerage: IProductInventory;
    Logo: IImageRef;
    Cover: IImageRef;
    When: IWhen;
}

//

export interface IProductInventory {
    Status: string;
    Unit: string;
    Price: number;
    Inventory: number;
}

// simple

export interface IProductSimple {
    ID: string;
    ShopID: string;
    Name: string;
    Slug: string;
    SKU: string;
    Group: string;
    Class: string;
    Method: string;
    Type: string;
    Marketplace: IProductInventory;
    Brokerage: IProductInventory;
    Logo: IImageRef;
}
