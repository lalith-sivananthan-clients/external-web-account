import { ImageStatusEnum } from '@app/models/_shared/shared.enums';

export interface IAuthenticationJWT {
	Username: string;
	Confirmed: boolean;
	PasswordSet: boolean;
	ID: string;
	NotBefore: number;
	ExpiresOn: number;
    AvatarID: string;
    AvatarStatus: ImageStatusEnum;
	ConnectionFacebook: boolean;
	ConnectionGoogle: boolean;
	ConnectionLinkedIn: boolean;
}
