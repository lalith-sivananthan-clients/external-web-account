declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';

import { URLConfig } from '@app/configs/config.consts';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeader, GetHeaderWithUser } from '@app/models/_shared/shared.functions';

@Injectable()
export class ConnectAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

	// facebook
	public PostConnectFacebook(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'facebook', $.param(body), GetHeader());
	}

	public PostAddFacebook(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'add/facebook', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public GetDisconnectFacebook(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIConnect + 'disconnect/facebook', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	// google
	public PostConnectGoogle(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'google', $.param(body), GetHeader());
	}

	public PostAddGoogle(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'add/google', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public GetDisconnectGoogle(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIConnect + 'disconnect/google', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	// linkedin
	public PostConnectLinkedIn(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'linkedin', $.param(body), GetHeader());
	}

	public PostAddLinkedIn(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIConnect + 'add/linkedin', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public GetDisconnectLinkedIn(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIConnect + 'disconnect/linkedin', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

}
