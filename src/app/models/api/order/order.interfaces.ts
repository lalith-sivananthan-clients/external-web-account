import { IShopSimple } from '@app/models/api/shop/shop.interfaces';
import { IProductSimple } from '@app/models/api/shop-product/product.interfaces';

// order

export interface IOrder {
    ID: string;
    AccountID: string;
    ShopID: string;
    Status: string;
    Shop: IShopSimple;
    Products: IOrderProduct[];
}

export interface IOrderProduct {
    ProductID: string;
    Product: IProductSimple;
    Quanitity: number;
}
