declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser } from '@app/models/_shared/shared.functions';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class OrderAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

    // order

    public PostOrderList(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APITransaction + 'order/list', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostOrderSingle(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APITransaction + 'order/single', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

}
