declare let $: any;

import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';
import { IImageCrop } from '@app/components/_shared/uploader/uploader.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser, GetUploadHeaderWithUser } from '@app/models/_shared/shared.functions';

import { URLConfig } from '@app/configs/config.consts';

@Injectable()
export class ShopAPI {

    constructor(
        private http: HttpClient,
        private accountModel: AccountModel
    ) { }

    public PostList(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'list', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingle(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleCreate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/create', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleDelete(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/delete', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    // create

    public PostShopCreateStep1(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/create/step-1', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShopCreateStep2(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/create/step-2', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShopCreateStep3(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/create/step-3', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShopCreateStep4(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/create/step-4', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    // profile

    public PostShopProfileBasicUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/profile/basic/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostShopProfileLinksUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/profile/links/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleProfileLogoUpload(file: File, options: IImageCrop, id: string): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('File', file);
        formData.append('ShopID', id);
        formData.append('XOffset', options.XOffset.toString());
        formData.append('YOffset', options.YOffset.toString());
        formData.append('Width', options.Width.toString());
        formData.append('Height', options.Height.toString());

        return this.http.post<IResponse>(URLConfig.APIShop + 'single/profile/logo/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleProfileCoverUpload(file: File, options: IImageCrop, id: string): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('File', file);
        formData.append('ShopID', id);
        formData.append('XOffset', options.XOffset.toString());
        formData.append('YOffset', options.YOffset.toString());
        formData.append('Width', options.Width.toString());
        formData.append('Height', options.Height.toString());

        return this.http.post<IResponse>(URLConfig.APIShop + 'single/profile/cover/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    // settings

    public PostSingleSettingsPrimaryContactNameUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/primary-contact/name/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsPrimaryContactDoBUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/primary-contact/dob/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsPrimaryContactContactUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/primary-contact/contact/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsBusinessDetailUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/business-detail/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsBusinessLicenseUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/business-license/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsBusinessContactAddressUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/business-contact/address/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    public PostSingleSettingsBusinessContactContactUpdate(body: Object): Observable<any> {
        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/business-contact/contact/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    //

    public PostSingleSettingsPrimaryContactUpload(file: File, id: string, type: string): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('File', file);
        formData.append('ShopID', id);
        formData.append('Type', type);

        return this.http.post<IResponse>(URLConfig.APIShop + 'single/settings/primary-contact/id/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
    }

    // public PostSingleSettingsBusinessLicenseLicenseUpload(file: File, id: string, type: string): Observable<any> {
    //     const formData: FormData = new FormData();
    //     formData.append('File', file);
    //     formData.append('ShopID', id);
    //     formData.append('Type', type);
    //
    //     return this.http.post<IResponse>(URLConfig.APIShop + 'settings/business-license/license/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
    // }

}
