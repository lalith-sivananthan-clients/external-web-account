declare let $: any;

import { Observable, Subject } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpRequest, HttpResponse } from '@angular/common/http';

import { IResponse } from '@app/models/_shared/shared.interfaces';
import { IImageCrop } from '@app/components/_shared/uploader/uploader.interfaces';

import { URLConfig } from '@app/configs/config.consts';

import { AccountModel } from '@app/models/local/account/account.classes';

import { GetHeaderWithUser, GetUploadHeaderWithUser } from '@app/models/_shared/shared.functions';

@Injectable()
export class AccountAPI {

	constructor(
		private http: HttpClient,
		private accountModel: AccountModel
	) { }

	public PostAccountConfirm(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'account/confirm', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostAccountConfirmResend(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'account/confirm/resend', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostEmailConfirm(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'email/confirm', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostEmailUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'email/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostUsernameUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'username/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostPasswordSet(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'password/set', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostPasswordUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'password/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	// profile : get

	public GetProfileAvatar(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIAccount + 'profile/avatar', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public GetProfileHeadline(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIAccount + 'profile/headline', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public GetProfileStory(): Observable<any> {
		return this.http.get<IResponse>(URLConfig.APIAccount + 'profile/story', GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	// profile : post

	public PostProfileHeadlineUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'profile/headline/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

	public PostProfileStoryUpdate(body: Object): Observable<any> {
		return this.http.post<IResponse>(URLConfig.APIAccount + 'profile/story/update', $.param(body), GetHeaderWithUser(this.accountModel.TokenLocalValue));
	}

    public PostProfileAvatarUpload(file: File, options: IImageCrop): Observable<any> {
        const url = URLConfig.APIAccount + 'profile/avatar/upload';

        const formData: FormData = new FormData();
        formData.append('File', file);
        formData.append('XOffset', options.XOffset.toString());
        formData.append('YOffset', options.YOffset.toString());
        formData.append('Width', options.Width.toString());
        formData.append('Height', options.Height.toString());

        return this.http.post<IResponse>(URLConfig.APIAccount + 'profile/avatar/upload', formData, GetUploadHeaderWithUser(this.accountModel.TokenLocalValue));
    }
}
