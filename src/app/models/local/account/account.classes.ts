declare let jwt_decode: any;

import { Injectable } from '@angular/core';

import { IAuthenticationJWT } from '@app/models/api/authentication/authentication.interfaces';
import { IAccount, IAccountConnections, IAccountToken } from './account.interfaces';
import { IMessage } from '@app/services/core/language/langauge.interfaces';
import { IImage } from '@app/models/_shared/shared.interfaces';

import { ImageStatusEnum } from '@app/models/_shared/shared.enums';

import { CookieService } from 'ngx-cookie-service';
import { StorageLocalService } from '@app/services/core/storage/local.classes';

import { AccountSignedInEvent } from './account.variables';
import { MessengerGlobalEvent } from '@app/services/core/messenger/messenger.variables';
import { StorageConfig } from '@app/configs/config.consts';

@Injectable()
export class AccountModel {

	constructor(
		private storageCookieService: CookieService,
		private storageLocalService: StorageLocalService
	) {
        this.Check();
	}

	// set

	set Account(data: IAccount) {
		this.storageLocalService.Save('account', data);
	}

	set AccountID(data: string) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.ID = data;

		this.storageLocalService.Save('account', _account);
	}

	set AccountUsername(data: string) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.Username = data;

		this.storageLocalService.Save('account', _account);
	}

	set AccountConfirmed(data: boolean) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.Confirmed = data;

		this.storageLocalService.Save('account', _account);
	}

	set AccountPasswordSet(data: boolean) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.PasswordSet = data;

		this.storageLocalService.Save('account', _account);
	}

    set AccountAvatarID(data: string) {
        let _account = <IAccount>this.storageLocalService.Get('account');
        _account.Avatar.MediaID = data;

        this.storageLocalService.Save('account', _account);
    }

    set AccountAvatarStatus(data: ImageStatusEnum) {
        let _account = <IAccount>this.storageLocalService.Get('account');
        _account.Avatar.Status = data;

        this.storageLocalService.Save('account', _account);
    }

	set AccountConnectionsFacebook(data: boolean) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.Connections.Facebook = data;

		this.storageLocalService.Save('account', _account);
	}

	set AccountConnectionsGoogle(data: boolean) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.Connections.Google = data;

		this.storageLocalService.Save('account', _account);
	}

	set AccountConnectionsLinkedIn(data: boolean) {
		let _account = <IAccount>this.storageLocalService.Get('account');
		_account.Connections.LinkedIn = data;

		this.storageLocalService.Save('account', _account);
	}

	set TokenLocal(data: IAccountToken) {
		this.storageLocalService.Save('token', data);
	}

	set TokenLocalValue(data: string) {
		let _token = <IAccountToken>this.storageLocalService.Get('token');
		_token.Value = data;

		this.storageLocalService.Save('token', _token);
	}

    set TokenCookie(data: string) {
        this.storageCookieService.set('token', data, null, StorageConfig.Path, StorageConfig.Domain);
    }

	// get

	get Account(): IAccount {
		return <IAccount>this.storageLocalService.Get('account');
	}

	get AccountID(): string {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.ID;
	}

	get AccountUsername(): string {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.Username;
	}

	get AccountConfirmed(): boolean {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.Confirmed;
	}

	get AccountPasswordSet(): boolean {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.PasswordSet;
	}

    get AccountAvatarID(): string {
        let _account = <IAccount>this.storageLocalService.Get('account');

        return _account.Avatar.MediaID;
    }

    get AccountAvatarStatus(): ImageStatusEnum {
        let _account = <IAccount>this.storageLocalService.Get('account');

        return _account.Avatar.Status;
    }

	get AccountConnectionsFacebook(): boolean {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.Connections.Facebook;
	}

	get AccountConnectionsGoogle(): boolean {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.Connections.Google;
	}

	get AccountConnectionsLinkedIn(): boolean {
		let _account = <IAccount>this.storageLocalService.Get('account');

		return _account.Connections.LinkedIn;
	}

	get TokenLocal(): IAccountToken {
		return <IAccountToken>this.storageLocalService.Get('token');
	}

	get TokenLocalValue(): string {
		let _token = <IAccountToken>this.storageLocalService.Get('token');

		return _token.Value;
	}

    get TokenCookie(): string {
        return <string>this.storageCookieService.get('token');
    }

	// func : public

    public Reset(): void {
        this.storageCookieService.delete('token', StorageConfig.Path, StorageConfig.Domain);
        this.storageLocalService.Save('account', <IAccount>{});
        this.storageLocalService.Save('token', <IAccountToken>{});
    }

    public Check(): boolean {
        if (this.IsLoggedIn() === true) {
            if (this.TokenCookie !== this.TokenLocalValue) {
                this.SaveLocal();
            }

            return true;
        } else if (this.IsCookieSet() === true && this.IsLocalSet() === false) {
            this.SaveLocal();

            return true;
        }
        this.Logout();

        return false;
    }

    public SaveCookieAndLocal(token: string): void {
	    this.TokenCookie = token;
	    this.SaveLocal();
    }

    public SaveLocal(): void {
        const _decoded: IAuthenticationJWT = jwt_decode(this.TokenCookie);

        this.storageLocalService.Save('account', <IAccount>{Avatar: <IImage>{}, Connections: <IAccountConnections>{}});
        this.storageLocalService.Save('token', <IAccountToken>{});

        this.TokenLocalValue = this.TokenCookie;

        this.AccountID = _decoded.ID;
        this.AccountUsername = _decoded.Username;
        this.AccountConfirmed = _decoded.Confirmed;
        this.AccountPasswordSet = _decoded.PasswordSet;
        this.AccountAvatarID = _decoded.AvatarID || '';
        this.AccountAvatarStatus = <ImageStatusEnum>_decoded.AvatarStatus || ImageStatusEnum.NotSet;
        this.AccountConnectionsFacebook = _decoded.ConnectionFacebook;
        this.AccountConnectionsGoogle = _decoded.ConnectionGoogle;
        this.AccountConnectionsLinkedIn = _decoded.ConnectionLinkedIn;
    }

    public IsCookieSet(): boolean {
        return this.TokenCookie.length > 0;
    }

    public IsLocalSet(): boolean {
        const _account = <IAccount>this.storageLocalService.Get('account');
        const _tokenLocal = <IAccountToken>this.storageLocalService.Get('token');

        if (typeof _account === 'undefined' || typeof _tokenLocal === 'undefined' || _account === null || _tokenLocal === null) {
            return false;
        } else if (_account.ID === null || _tokenLocal.Value === null) {
            return false;
        }

        return true;
	}

    public IsLoggedIn(): boolean {
	    return (this.IsCookieSet() && this.IsLocalSet());
    }

	public Logout(): void {
        this.Reset();

		AccountSignedInEvent.next(true);
		MessengerGlobalEvent.next(<IMessage>{});
	}

}
