import { Component, ViewEncapsulation, Input, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'sprout-shared-pagination',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './pagination.component.html',
	styleUrls: [
		'./pagination.component.css'
	]
})
export class SharedPaginationComponent implements OnInit, OnChanges {

	// input

	@Input()
	sproutPageCurrent: number;

	@Input()
	sproutPageTotal: number;

	@Input()
	sproutPageLink: string;

	//

	public enablePrevious: boolean;
	public enableNext: boolean;

	constructor(private router: Router) {
		this.enablePrevious = true;
		this.enableNext = true;
	}

	ngOnInit() {
	}

	ngOnChanges() {
		this.enablePrevious = true;
		this.enableNext = true;

		if (this.sproutPageCurrent === 1) {
			this.enablePrevious = false;

			if (this.sproutPageTotal === 1) {
				this.enableNext = false;
			}
		} else {
			if (this.sproutPageTotal === this.sproutPageCurrent) {
				this.enableNext = false;
			}
		}
	}

}
