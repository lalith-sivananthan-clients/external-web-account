import { Component, ViewEncapsulation, Input, Output, EventEmitter, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core';

import { UploadingStatusEvent } from '@app/components/_shared/uploader/uploader.variables';

@Component({
	selector: 'sprout-shared-uploader-simple',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './uploader-simple.component.html',
	styleUrls: [
		'./uploader-simple.component.css'
	]
})
export class SharedUploaderSimpleComponent implements OnInit, OnDestroy {

	// input

	@Input()
	sproutFile: File;

	@Input()
	sproutFiles: Set<File>;

	@Input()
	sproutFileMultiple: boolean;

	// output

	@Output()
	sproutFileChange: EventEmitter<File> = new EventEmitter<File>();

	@Output()
	sproutFilesChange: EventEmitter<Set<File>> = new EventEmitter<Set<File>>();

	//

	@Output()
	sproutTriggerUpload: EventEmitter<any> = new EventEmitter<any>();

	// child

	@ViewChild('inputSelector')
	inputSelector: ElementRef;

	// _ events

	private _subscribeUploadingStatusEvent: any;

	//

	constructor() {
		this.sproutFile = null;
		this.sproutFiles = new Set();
	}

	ngOnInit() {
		this._subscribeUploadingStatusEvent = UploadingStatusEvent.subscribe((data) => {
		});
	}

	ngOnDestroy() {
		this._subscribeUploadingStatusEvent.unsubscribe();
	}

	public showName() {
		if (this.sproutFile) {
			return true;
		}

		return false;
	}

	//

	public changeFile() {
		if (this.inputSelector.nativeElement.files.length === 0) {
			if (this.sproutFile !== null) {
				return;
			}

			return;
		}

		if (this.sproutFileMultiple === true) {
			const files: { [key: string]: File } = this.inputSelector.nativeElement.files;

			for (let key in files) {
				if (!isNaN(parseInt(key, 10))) {
					this.sproutFiles.add(files[key]);
				}
			}
		} else {
			this.sproutFile = this.inputSelector.nativeElement.files[0];
		}
	}

	public openUploader(event, element: HTMLElement) {
		element.click();
	}

	// trigger

	public triggerUpload() {
		if (this.sproutFileMultiple === true) {
		} else {
			this.sproutTriggerUpload.emit({
				file: this.sproutFile
			});
		}
	}

}
