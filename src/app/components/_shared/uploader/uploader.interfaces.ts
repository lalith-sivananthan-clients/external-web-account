export interface IImageCrop {
	XOffset: number;
	YOffset: number;
	Width: number;
	Height: number;
}
