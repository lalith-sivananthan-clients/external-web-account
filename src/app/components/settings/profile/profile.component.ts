import { Component, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'sprout-settings-profile',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './profile.component.html',
	styleUrls: [
		'./profile.component.css'
	]
})
export class SettingsProfileComponent {
}
