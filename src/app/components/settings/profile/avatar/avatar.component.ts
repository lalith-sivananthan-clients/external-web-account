import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';
import { IImageCrop } from '@app/components/_shared/uploader/uploader.interfaces';
import { IValidatorImageRules } from '@app/services/core/validator/image.interfaces';

import { ImageStatusEnum } from '@app/models/_shared/shared.enums';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { URLConfig } from '@app/configs/config.consts';


@Component({
	selector: 'sprout-settings-profile-avatar',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './avatar.component.html',
	styleUrls: [
		'./avatar.component.css'
	]
})
export class SettingsProfileAvatarComponent implements OnInit {

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isActive: boolean;

    // message

    public messages: IMessages;
    public message: IMessage;

    //

    public mediaURL: string;

    public file: File;
    public fileCrop: IImageCrop;
    public fileMultiple: boolean;
    public fileRules: IValidatorImageRules;

    constructor(
        public accountModel: AccountModel,
        public accountAPI: AccountAPI
    ) {
    }

    ngOnInit() {
        this._init();
    }

    // func

    public imageStatus(data: ImageStatusEnum) {
        if (data === ImageStatusEnum.NotSet && this.accountModel.AccountAvatarStatus === ImageStatusEnum.NotSet) {
            return true;
        }

        if (data === ImageStatusEnum.Set && this.accountModel.AccountAvatarStatus === ImageStatusEnum.Set) {
            return true;
        }

        if (data === ImageStatusEnum.Processing && this.accountModel.AccountAvatarStatus === ImageStatusEnum.Processing) {
            return true;
        }

        return false;
    }

    public upload(event: any) {
        console.log('start');
        console.log('data', event);

        if (typeof event !== 'undefined') {
            this.file = <File>event.file;
            this.fileCrop = event.crop;

            this.accountAPI.PostProfileAvatarUpload(
                this.file,
                this.fileCrop
            ).subscribe((response: IResponse) => {
                    let _result: IResult = response.Result;

                    if (response.Success === true && _result.Code === 1) {
                        this._init();

                        this.accountModel.AccountAvatarID = _result.Data;
                        this.accountModel.AccountAvatarStatus = ImageStatusEnum.Set;

                        this.message = this.messages.GlobalBasic.Success;
                        this.message.Show = true;
                    } else {
                        this.message = this.messages.GlobalBasic.Error;
                        this.message.Show = true;
                    }

                    this.isLoading = false;
                },
                error => {
                    this.message = this.messages.GlobalBasic.Error;
                    this.message.Show = true;

                    this.isLoading = false;
                });
        }
    }

    // func : toggle

    public toggleActive(data: boolean) {
        this.isActive = data;

        if (this.isActive === false) {
            this._init();
        }
    }

    // func : _

    private _init() {
        this.isActive = false;
        this.isLoading = false;

        this.messages = LanguageService.GetMessages('en');
        this.message = <IMessage>{};

        this.mediaURL = URLConfig.GatewayMedia;

        this.file = null;
        this.fileCrop = <IImageCrop>{};
        this.fileMultiple = false;
        this.fileRules = {
            Ratio: 1,
            MinWidth: 50,
            MaxWidth: 0,
            MinHeight: 50,
            MaxHeight: 0,
            Width: 0,
            Height: 0
        };
    }

}
