import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-settings-profile-headline',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './headline.component.html',
	styleUrls: [
		'./headline.component.css'
	]
})
export class SettingsProfileHeadlineComponent implements OnInit {

	// options

	public isLoading: boolean;
	public isError: boolean;
	public isReady: boolean;
	public isActive: boolean;

	// form

	public form: FormGroup;
	public inputHeadline: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	//

	public headline: string;

	constructor(
		public accountModel: AccountModel,
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();
	}

	// func

	public submit() {
		if (this.isLoading === true) {
			return;
		}

		this.isLoading = true;
		this.message = <IMessage>{};

		this.accountAPI.PostProfileHeadlineUpdate({
			Headline: this.inputHeadline.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this._init();
					this._formInit();

					this.message = this.messages.GlobalBasic.Success;
					this.message.Show = true;
				} else {
					this.message = this.messages.GlobalBasic.Error;
					this.message.Show = true;
				}

				this.isLoading = false;
			},
			error => {
				this.message = this.messages.GlobalBasic.Error;
				this.message.Show = true;

				this.isLoading = false;
			}
		);
	}

	// func : toggle

	public toggleActive(data: boolean) {
		this.isActive = data;

		if (this.isActive === false) {
			this._init();
			this._formInit();
		}
	}

	// func : _

	private _get() {
		this.accountAPI.GetProfileHeadline().subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.headline = _result.Data;
					this._formInit();
				} else {
					this.message = this.messages.GlobalBasic.Server;
					this.message.Show = true;
				}
			}, () => {
				this.message = this.messages.GlobalBasic.Server;
				this.message.Show = true;
			}
		);
	}

	private _init() {
		this.isActive = false;
		this.isLoading = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};

		this._get();
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputHeadline = new FormField('headline', 'Headline', this.headline, [
		]);

		this.form.SetFields([ this.inputHeadline ]);
		this.form.SetParent();
	}

}
