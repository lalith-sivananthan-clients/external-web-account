import {IShopSimple} from '@app/models/api/shop/shop.interfaces';

declare let _: any;

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';
import {IOrder, IOrderProduct} from '@app/models/api/order/order.interfaces';
import { IProductSimple } from '@app/models/api/shop-product/product.interfaces';

import { LanguageService } from '@app/services/core/language/language.classes';

import { OrderAPI } from '@app/models/api/order/order.classes';

@Component({
	selector: 'sprout-settings-orders-single',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './single.component.html',
	styleUrls: [
		'./single.component.css'
	]
})
export class SettingsOrdersSingleComponent implements OnInit, OnDestroy {

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isOrderSet: boolean;

    // message

    public messages: IMessages;
    public message: IMessage;

    //

    public order: IOrder;

    // _ events

    private _subscribeParams: any;

    // _ params

    private _id: number;

    constructor(
        private activatedRoute: ActivatedRoute,
        private orderAPI: OrderAPI
    ) {
        this._init();
    }

    ngOnInit() {
        this._subscribeParams = this.activatedRoute.params.subscribe(params => {
            this._id = params['orderID'];

            this._get();
        });
    }

    ngOnDestroy() {
        this._subscribeParams.unsubscribe();
    }

    // func

    getPrice(data: IOrderProduct): number {
        if (! this.isOrderSet) {
            return;
        }

        return _.round((data.Product.Marketplace.Price * data.Quanitity) / 100, 2);
    }

    getPriceTotal(): number {
        if (! this.isOrderSet) {
            return;
        }

        let _value = 0;

        for (let iProducts = 0; iProducts < this.order.Products.length; iProducts++) {
            _value = _value + (this.order.Products[iProducts].Product.Marketplace.Price * this.order.Products[iProducts].Quanitity);
        }

        return _.round(_value / 100, 2);
    }

    // func : _

    _get() {
        if (this.isLoading === true) {
            return;
        }

        this.isLoading = true;
        this.message = <IMessage>{};

        this.orderAPI.PostOrderSingle({
            OrderID: this._id
        }).subscribe(
            (response: IResponse) => {
                let _result: IResult = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.order = _result.Data;

                    this.isOrderSet = true;
                    this.isReady = true;
                } else {
                    this.message = this.messages.GlobalBasic.Server;
                    this.message.Show = true;
                }

                this.isLoading = false;
            },
            error => {
                this.message = this.messages.GlobalBasic.Server;
                this.message.Show = true;

                this.isLoading = false;
            }
        );
    }

    _init() {
        this.isError = false;
        this.isReady = false;
        this.isOrderSet = false;

        this.messages = LanguageService.GetMessages('en');
        this.message = <IMessage>{};

        this._id = this._id || null;

        this.order = <IOrder>{
            Shop: <IShopSimple>{}
        };
    }
}
