declare let _: any;

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';
import { IOrder } from '@app/models/api/order/order.interfaces';

import { LanguageService } from '@app/services/core/language/language.classes';

import { OrderAPI } from '@app/models/api/order/order.classes';

@Component({
	selector: 'sprout-settings-orders-list',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './list.component.html',
	styleUrls: [
		'./list.component.css'
	]
})
export class SettingsOrdersListComponent implements OnInit, OnDestroy {

    // options

    public isLoading: boolean;
    public isError: boolean;
    public isReady: boolean;
    public isOrderSet: boolean;

    // message

    public messages: IMessages;
    public message: IMessage;

    //

    public orders: IOrder[];

    // _ events

    private _subscribeParams: any;

    // _ params

    private page: number;

    constructor(
        private activatedRoute: ActivatedRoute,
        private orderAPI: OrderAPI
    ) {
        this._init();
    }

    ngOnInit() {
        this._subscribeParams = this.activatedRoute.params.subscribe(params => {
            let page = parseInt(params['page'], 10);

            if (! isNaN(page)) {
                this.page = page;
            }

            this._get();
        });
    }

    ngOnDestroy() {
        this._subscribeParams.unsubscribe();
    }

    // func

    getValue(data: IOrder): number {
        if (! this.isOrderSet) {
            return;
        }

        let _value = 0;

        for (let iProducts = 0; iProducts < data.Products.length; iProducts++) {
            _value = _value + (data.Products[iProducts].Product.Marketplace.Price * data.Products[iProducts].Quanitity);
        }

        return _.round(_value / 100, 2);
    }

    // func : _

    _get() {
        if (this.isLoading === true) {
            return;
        }

        this.isLoading = true;
        this.message = <IMessage>{};

        this.orderAPI.PostOrderList({
            Page: this.page
        }).subscribe(
            (response: IResponse) => {
                let _result: IResult = response.Result;

                if (response.Success === true && _result.Code === 1) {
                    this.orders = _result.Data;

                    this.isOrderSet = true;
                    this.isReady = true;
                } else {
                    this.message = this.messages.GlobalBasic.Server;
                    this.message.Show = true;
                }

                this.isLoading = false;
            },
            error => {
                this.message = this.messages.GlobalBasic.Server;
                this.message.Show = true;

                this.isLoading = false;
            }
        );
    }

    _init() {
        this.isError = false;
        this.isReady = false;
        this.isOrderSet = false;

        this.messages = LanguageService.GetMessages('en');
        this.message = <IMessage>{};

        this.page = this.page || 1;

        this.orders = [];
    }
}
