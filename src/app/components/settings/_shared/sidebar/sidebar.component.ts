import { Component, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'sprout-settings-shared-sidebar',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './sidebar.component.html',
	styleUrls: [
		'./sidebar.component.css'
	]
})
export class SettingsSharedSidebarComponent {
}
