import { Component, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'sprout-settings-preferences',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './preferences.component.html',
	styleUrls: [
		'./preferences.component.css'
	]
})
export class SettingsPreferencesComponent {
}
