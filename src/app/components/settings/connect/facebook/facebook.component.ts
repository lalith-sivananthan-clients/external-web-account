import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { ConnectAPI } from '@app/models/api/connect/connect.classes';

import { URLConfig } from '@app/configs/config.consts';

import { PopUp } from '@app/services/core/helpers/helpers.functions';

@Component({
	selector: 'sprout-settings-connect-facebook',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './facebook.component.html',
	styleUrls: [
		'./facebook.component.css'
	]
})
export class SettingsConnectFacebookComponent implements OnInit, OnDestroy {

	// options

	public isLoading: boolean;
	public isConnected: boolean;
	public confirmDisconnect: boolean;

	//

	private _popup: Window;
	private _storageListener: any;

	constructor(
		public accountModel: AccountModel,
		public connectAPI: ConnectAPI
	) {
		this._init();
	}

	ngOnInit() {
		window.addEventListener('storage', this._storageListener, false);
	}

	ngOnDestroy() {
		window.removeEventListener('storage', this._storageListener, false);
	}

	// func

	connect() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'facebook/', 700, 610);
	}

	disconnect() {
		this.isLoading = true;

		this.connectAPI.GetDisconnectFacebook().subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.accountModel.AccountConnectionsFacebook = false;
					this._init();
				} else {
					this.isLoading = false;
					// TODO
					// error message
				}
			},
			error => {
				this.isLoading = false;
				// TODO
				// error message
			}
		);
	}

	// func : toggle

	toggleDisconnect() {
		this.confirmDisconnect = ! this.confirmDisconnect;
	}

	// func : _

	_init() {
		this.isLoading = false;
		this.isConnected = this.accountModel.AccountConnectionsFacebook;
		this.confirmDisconnect = false;

		this._storageListener = () => {
			this._init();
		};
	}

}
