import { Component, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'sprout-settings-connect',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './connect.component.html',
	styleUrls: [
		'./connect.component.css'
	]
})
export class SettingsConnectComponent {
}
