import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { AccountModel } from '@app/models/local/account/account.classes';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorOptionEnum, ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-settings-account-username',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './username.component.html',
	styleUrls: [
		'./username.component.css'
	]
})
export class SettingsAccountUsernameComponent implements OnInit {

	// options

	public isLoading: boolean;
	public isActive: boolean;

	// form

	public form: FormGroup;
	public inputUsername: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	constructor(
		public accountModel: AccountModel,
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();
	}

	// func

	public submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.message = <IMessage>{};

		this.accountAPI.PostUsernameUpdate({
			Username: this.inputUsername.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.accountModel.AccountUsername = this.inputUsername.Value;

					this._init();
					this._formInit();

					this.message = this.messages.Account.UsernameUpdateSuccess;
					this.message.Show = true;
				} else {
					this.message = this.messages.Account.UsernameUpdateError;
					this.message.Show = true;
				}

				this.isLoading = false;
			},
			error => {
				this.message = this.messages.Account.UsernameUpdateError;
				this.message.Show = true;

				this.isLoading = false;
			}
		);
	}

	// func : toggle

	public toggleActive(data: boolean) {
		this.isActive = data;

		if (this.isActive === false) {
			this._init();
			this._formInit();
		}
	}

	// func : _

	private _init() {
		this.isActive = false;
		this.isLoading = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputUsername = new FormField('username', 'Username', this.accountModel.AccountUsername, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThanOrEqualTo,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 3 }
				]
			},
			{ Name: ValidatorRuleEnum.Username, Options: [] },
			{ Name: ValidatorRuleEnum.UsernameUnique, Options: [] }
		]);

		this.form.SetFields([ this.inputUsername ]);
		this.form.SetParent();
	}

}
