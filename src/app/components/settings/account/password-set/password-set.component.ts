import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { AccountModel } from '@app/models/local/account/account.classes';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorOptionEnum, ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-settings-account-password-set',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './password-set.component.html',
	styleUrls: [
		'./password-set.component.css'
	]
})
export class SettingsAccountPasswordSetComponent implements OnInit {

	// options

	public isLoading: boolean;
	public isActive: boolean;

	// form

	public form: FormGroup;
	public inputPassword: FormField;
	public inputPasswordConfirm: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	constructor(
		public accountModel: AccountModel,
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();
	}

	// func

	submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.message = <IMessage>{};

		this.accountAPI.PostPasswordSet({
			Password: this.inputPassword.Value,
			PasswordConfirm: this.inputPasswordConfirm.Value,
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.accountModel.AccountPasswordSet = true;

					this._init();
					this._formInit();

					this.message = this.messages.Account.PasswordUpdateSuccess;
					this.message.Show = true;
				} else {
					this.message = this.messages.Account.PasswordUpdateError;
					this.message.Show = true;
				}

				this.isLoading = false;
			},
			error => {
				this.message = this.messages.Account.PasswordUpdateError;
				this.message.Show = true;

				this.isLoading = false;
			}
		);
	}

	// func : toggle

	toggleActive(data: boolean) {
		this.isActive = data;

		if (this.isActive === false) {
			this._init();
			this._formInit();
		}
	}

	// func : _

	private _init() {
		this.isActive = false;
		this.isLoading = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputPassword = new FormField('password', 'Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThanOrEqualTo,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 6 }
				]
			},
			{
				Name: ValidatorRuleEnum.Confirm,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'passwordConfirm' }
				]
			}
		]);

		this.inputPasswordConfirm = new FormField('passwordConfirm', 'Confirm Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.ConfirmFor,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'password' }
				]
			}
		]);

		this.form.SetFields([ this.inputPassword, this.inputPasswordConfirm ]);
		this.form.SetParent();
	}

}
