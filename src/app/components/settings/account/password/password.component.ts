import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorOptionEnum, ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-settings-account-password',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './password.component.html',
	styleUrls: [
		'./password.component.css'
	]
})
export class SettingsAccountPasswordComponent implements OnInit {

	// options

	public isLoading: boolean;
	public isActive: boolean;

	// form

	public form: FormGroup;
	public inputPassword: FormField;
	public inputPasswordNew: FormField;
	public inputPasswordNewConfirm: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	constructor(
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();
	}

	// func

	submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.message = <IMessage>{};

		this.accountAPI.PostPasswordUpdate({
			PasswordNew: this.inputPasswordNew.Value,
			PasswordNewConfirm: this.inputPasswordNewConfirm.Value,
			Password: this.inputPassword.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this._init();
					this._formInit();

					this.message = this.messages.Account.PasswordUpdateSuccess;
					this.message.Show = true;
				} else {
					this.message = this.messages.Account.PasswordUpdateError;
					this.message.Show = true;
				}

				this.isLoading = false;
			},
			error => {
				this.message = this.messages.Account.PasswordUpdateError;
				this.message.Show = true;

				this.isLoading = false;
			}
		);
	}

	// func : toggle

	toggleActive(data: boolean) {
		this.isActive = data;

		if (this.isActive === false) {
			this._init();
			this._formInit();
		}
	}

	// func : _

	private _init() {
		this.isActive = false;
		this.isLoading = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputPassword = new FormField('password', 'Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
		]);

		this.inputPasswordNew = new FormField('passwordNew', 'New Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThanOrEqualTo,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 6 }
				]
			},
			{
				Name: ValidatorRuleEnum.Confirm,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'passwordNewConfirm' }
				]
			}
		]);

		this.inputPasswordNewConfirm = new FormField('passwordNewConfirm', 'Confirm New Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.ConfirmFor,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'passwordNew' }
				]
			}
		]);

		this.form.SetFields([ this.inputPassword, this.inputPasswordNew, this.inputPasswordNewConfirm ]);
		this.form.SetParent();
	}

}
