import { Component, ViewEncapsulation } from '@angular/core';

import { AccountModel } from '@app/models/local/account/account.classes';

@Component({
	selector: 'sprout-settings-account',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './account.component.html',
	styleUrls: [
		'./account.component.css'
	]
})
export class SettingsAccountComponent {

	// _ events

	constructor(public accountModel: AccountModel) {
	}

}
