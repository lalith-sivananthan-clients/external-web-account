import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-settings-account-email',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './email.component.html',
	styleUrls: [
		'./email.component.css'
	]
})
export class SettingsAccountEmailComponent implements OnInit {

	// options

	public isLoading: boolean;
	public isActive: boolean;

	// form

	public form: FormGroup;
	public inputEmail: FormField;

	// message

	public messages: IMessages;
	public message: IMessage;

	constructor(
		public accountModel: AccountModel,
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this._init();
		this._formInit();
	}

	// func

	public submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.message = <IMessage>{};

		this.accountAPI.PostEmailUpdate({
			Email: this.inputEmail.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this._init();
					this._formInit();

					this.message = this.messages.Account.EmailUpdateSuccess;
					this.message.Show = true;
				} else {
					this.message = this.messages.Account.EmailUpdateError;
					this.message.Show = true;
				}

				this.isLoading = false;
			},
			error => {
				this.message = this.messages.Account.EmailUpdateError;
				this.message.Show = true;

				this.isLoading = false;
			}
		);
	}

	// func : toggle

	public toggleActive(data: boolean) {
		this.isActive = data;

		if (this.isActive === false) {
			this._init();
			this._formInit();
		}
	}

	// func : _

	private _init() {
		this.isActive = false;
		this.isLoading = false;

		this.messages = LanguageService.GetMessages('en');
		this.message = <IMessage>{};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputEmail = new FormField('email', 'Email', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{ Name: ValidatorRuleEnum.Email, Options: [] },
			{ Name: ValidatorRuleEnum.EmailUnique, Options: [] }
		]);

		this.form.SetFields([ this.inputEmail ]);
		this.form.SetParent();
	}

}
