import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

@Component({
	selector: 'sprout-forgot-password',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './forgot-password.component.html',
	styleUrls: [
		'./forgot-password.component.css'
	]
})
export class ForgotPasswordComponent {

	// options

	public isLoading: boolean;
	public isError: boolean;

	// form

	public form: FormGroup;
	public inputEmail: FormField;

	constructor(
		public router: Router,
		public authenticationAPI: AuthenticationAPI
	) {
		this._init();
		this._formInit();
	}

	// func

	submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.isError = false;

		this.authenticationAPI.PostPasswordForgot({
			Email: this.inputEmail.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.router.navigate(['/sign-in']).catch(() => {});
				}

				this.isLoading = false;
				this.isError = true;
			},
			error => {
				this.isLoading = false;
				this.isError = true;
			}
		);
	}

	// func : _

	_init() {
		this.isLoading = false;
		this.isError = false;
	}

	_formInit() {
		this.form = new FormGroup();

		this.inputEmail = new FormField('Email', 'Email', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{ Name: ValidatorRuleEnum.Email, Options: [] }
		]);

		this.form.SetFields([ this.inputEmail ]);
		this.form.SetParent();
	}

}
