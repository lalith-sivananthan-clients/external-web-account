import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { AccountAPI } from '@app/models/api/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';
import { MessagerService } from '@app/services/core/messenger/messenger.classes';

@Component({
	selector: 'sprout-confirm',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './confirm.component.html',
	styleUrls: [
		'./confirm.component.css'
	]
})
export class ConfirmComponent implements OnInit, OnDestroy {

	// events

	private subscribeParams: any;

	// message

	public messages: IMessages = LanguageService.GetMessages('en');
	public message: IMessage = <IMessage>{};

	//

	private token: string;
	private type: string;

	constructor(
		public route: ActivatedRoute,
		public messagerService: MessagerService,
		public accountModel: AccountModel,
		public accountAPI: AccountAPI
	) {
	}

	ngOnInit() {
		this.subscribeParams = this.route.params.subscribe(params => {
			this.token = params['token'];
			this.type = params['type'];

			switch (this.type) {
				case 'account': {
					this.accountAPI.PostAccountConfirm({
						Token: this.token
					}).subscribe(
						(response: IResponse) => {
							let _result: IResult = response.Result;

							if (response.Success === true && _result.Code === 1) {
								this.message =  this.messages.Account.ConfirmSuccess;
								this.accountModel.AccountConfirmed = true;
								this.messagerService.CheckGlobal();
							} else {
								this.message = this.messages.Account.ConfirmError;
							}
						},
						error => {
							this.message = this.messages.Account.ConfirmError;
						}
					);

					break;
				}

				case 'email': {
					this.accountAPI.PostEmailConfirm({
						Token: this.token
					}).subscribe(
						(response: IResponse) => {
							let _result: IResult = response.Result;

							if (response.Success === true && _result.Code === 1) {
								this.message =  this.messages.Account.ConfirmEmailSuccess;
								this.messagerService.CheckGlobal();
							} else {
								this.message = this.messages.Account.ConfirmEmailError;
							}
						},
						error => {
							this.message = this.messages.Account.ConfirmEmailError;
						}
					);

					break;
				}
			}
		});
	}

	ngOnDestroy() {
		this.subscribeParams.unsubscribe();
	}

}
