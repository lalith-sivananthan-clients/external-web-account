import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'sprout-dashboard',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './dashboard.component.html',
    styleUrls: [
        './dashboard.component.css'
    ]
})
export class DashboardComponent {

    constructor() {}

}
