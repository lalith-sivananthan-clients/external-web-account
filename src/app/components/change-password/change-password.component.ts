import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorRuleEnum, ValidatorOptionEnum } from '@app/services/core/validator/validator.enums';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';

@Component({
	selector: 'sprout-change-password',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './change-password.component.html',
	styleUrls: [
		'./change-password.component.css'
	]
})
export class ChangePasswordComponent implements OnInit, OnDestroy {

	// options

	public isLoading: boolean;
	public isError: boolean;

	// form

	public form: FormGroup;
	public inputPassword: FormField;
	public inputPasswordConfirm: FormField;

	// _

	private _token: string;

	// _ events

	private _subscribeParams: any;

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private authenticationAPI: AuthenticationAPI
	) {
		this._init();
		this._formInit();
	}

	ngOnInit() {
		this._subscribeParams = this.activatedRoute.params.subscribe(params => {
			this._token = params['token'];
		});
	}

	ngOnDestroy() {
		this._subscribeParams.unsubscribe();
	}

	// func

	public submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.isError = false;

		this.authenticationAPI.PostPasswordChange({
			Token: this._token,
			Password: this.inputPassword.Value,
			PasswordConfirm: this.inputPasswordConfirm.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.router.navigate(['/sign-in']).catch(() => {});
				}

				this.isLoading = false;
				this.isError = true;
			},
			error => {
				this.isLoading = false;
				this.isError = true;
			}
		);
	}

	// func : _

	private _init() {
		this.isLoading = false;
		this.isError = false;

		this._token = this._token || '';
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputPassword = new FormField('password', 'Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThan,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 6 }
				]
			},
			{
				Name: ValidatorRuleEnum.Confirm,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'passwordConfirm' }
				]
			}
		]);

		this.inputPasswordConfirm = new FormField('passwordConfirm', 'Confirm Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.ConfirmFor,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'password' }
				]
			}
		]);

		this.form.SetFields([ this.inputPassword, this.inputPasswordConfirm ]);
		this.form.SetParent();
	}

}
