import { Component, ViewEncapsulation } from '@angular/core';

import { URLConfig } from '@app/configs/config.consts';

@Component({
    selector: 'sprout-nav-top',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './top.component.html',
    styleUrls: [
        './top.component.css'
    ]
})
export class NavTopComponent {

    public url: any;

    constructor() {
        this.url = URLConfig;
    }

}
