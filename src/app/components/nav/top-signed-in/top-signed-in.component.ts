import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { ImageStatusEnum } from '@app/models/_shared/shared.enums';

import { AccountModel } from '@app/models/local/account/account.classes';

import { URLConfig } from '@app/configs/config.consts';

@Component({
    selector: 'sprout-nav-top-signed-in',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './top-signed-in.component.html',
    styleUrls: [
        './top-signed-in.component.css'
    ]
})
export class NavTopSignedInComponent {

    //

    public isVisible: boolean;

    public url: any;

    constructor(
        public router: Router,
        public accountModel: AccountModel
    ) {
        this.isVisible = false;
        this.url = URLConfig;
    }

    // func

    public imageStatus(data: ImageStatusEnum) {
        if (data === ImageStatusEnum.NotSet && this.accountModel.AccountAvatarStatus === ImageStatusEnum.NotSet) {
            return true;
        }

        if (data === ImageStatusEnum.Set && this.accountModel.AccountAvatarStatus === ImageStatusEnum.Set) {
            return true;
        }

        if (data === ImageStatusEnum.Processing && this.accountModel.AccountAvatarStatus === ImageStatusEnum.Processing) {
            return true;
        }

        return false;
    }

    public menu() {
        this.isVisible = ! this.isVisible;
    }

    public logout() {
        this.accountModel.Logout();
        document.location.href = URLConfig.WebAccount;
    }

}
