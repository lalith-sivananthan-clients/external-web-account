import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';
import { IAuthenticationJWT } from '@app/models/api/authentication/authentication.interfaces';

import { ValidatorRuleEnum } from '@app/services/core/validator/validator.enums';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { URLConfig } from '@app/configs/config.consts';
import { AccountSignedInEvent } from '@app/models/local/account/account.variables';

import { PopUp } from '@app/services/core/helpers/helpers.functions';

@Component({
	selector: 'sprout-sign-in',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './sign-in.component.html',
	styleUrls: [
		'./sign-in.component.css'
	]
})
export class SignInComponent implements OnInit, OnDestroy {

	// options

	public isLoading: boolean;
	public isError: boolean;

	// form

	public form: FormGroup;
	public inputEmail: FormField;
	public inputPassword: FormField;

	// _

	private _popup: Window;
	private _storageListener: any;

	constructor(
		public router: Router,
		public accountModel: AccountModel,
		public authenticationAPI: AuthenticationAPI
	) {
		this._init();
		this._formInit();
	}

	ngOnInit() {
		window.addEventListener('storage', this._storageListener, false);
	}

	ngOnDestroy() {
		window.removeEventListener('storage', this._storageListener, false);
	}

	// func

	public submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.isError = false;

		this.authenticationAPI.PostSignIn({
			Email: this.inputEmail.Value,
			Password: this.inputPassword.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
                    this.accountModel.SaveCookieAndLocal(_result.Data);

					if (this.accountModel.IsLoggedIn() === true) {
						AccountSignedInEvent.next(true);
						this.router.navigate(['/dashboard']).catch(() => {});
					}
				}

				this.isLoading = false;
				this.isError = true;
			},
			error => {
				this.isLoading = false;
				this.isError = true;
			}
		);
	}

	// func : connect

	public connectFacebook() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'facebook/', 700, 610);
	}

	public connectGoogle() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'google/', 700, 610);
	}

	public connectLinkedIn() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'linkedin/', 700, 610);
	}

	// func : _

	private _init() {
		this.isLoading = false;
		this.isError = false;

		this._storageListener = () => {
			if (this.accountModel.IsLoggedIn() === true) {
				AccountSignedInEvent.next(true);
				this.router.navigate(['/explore/products']).catch(() => {});
			}
		};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputEmail = new FormField('Email', 'Email', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{ Name: ValidatorRuleEnum.Email, Options: [] }
		]);

		this.inputPassword = new FormField('password', 'Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] }
		]);

		this.form.SetFields([ this.inputEmail, this.inputPassword ]);
		this.form.SetParent();
	}

}
