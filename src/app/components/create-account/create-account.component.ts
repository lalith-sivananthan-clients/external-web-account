import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';
import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';

import { ValidatorRuleEnum, ValidatorOptionEnum } from '@app/services/core/validator/validator.enums';

import { FormField, FormGroup } from '@app/services/core/validator/form.classes';

import { PopUp } from '@app/services/core/helpers/helpers.functions';

import { URLConfig } from '@app/configs/config.consts';
import { AccountSignedInEvent } from '@app/models/local/account/account.variables';

@Component({
	selector: 'sprout-create-account',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './create-account.component.html',
	styleUrls: [
		'./create-account.component.css'
	]
})
export class CreateAccountComponent implements OnInit, OnDestroy {

	// options

	public isLoading: boolean;
	public isError: boolean;

	// form

	public form: FormGroup;
	public inputUsername: FormField;
	public inputEmail: FormField;
	public inputPassword: FormField;
	public inputPasswordConfirm: FormField;

	// _

	private _popup: Window;
	private _storageListener: any;

	constructor(
		public router: Router,
		public accountModel: AccountModel,
		public authenticationAPI: AuthenticationAPI
	) {
		this._init();
		this._formInit();
	}

	ngOnInit() {
		window.addEventListener('storage', this._storageListener, false);
	}

	ngOnDestroy() {
		window.removeEventListener('storage', this._storageListener, false);
	}

	// func

	public submit() {
		if (this.isLoading === true || ! this.form.IsValid) {
			return;
		}

		this.isLoading = true;
		this.isError = false;

		this.authenticationAPI.PostCreateAccount({
			Username: this.inputUsername.Value,
			Email: this.inputEmail.Value,
			Password: this.inputPassword.Value,
			PasswordConfirm: this.inputPasswordConfirm.Value
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.router.navigate(['/sign-in']).catch(() => {});
				}

				this.isLoading = false;
				this.isError = true;
			},
			error => {
				this.isLoading = false;
				this.isError = true;
			}
		);
	}

	// func : connect

	public connectFacebook() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'facebook/', 700, 610);
	}

	public connectGoogle() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'google/', 700, 610);
	}

	public connectLinkedIn() {
		this._popup = PopUp(URLConfig.GatewayConnect + 'linkedin/', 700, 610);
	}

	// func : _

	private _init() {
		this.isLoading = false;
		this.isError = false;

		this._storageListener = () => {
			if (this.accountModel.IsLoggedIn() === true) {
				AccountSignedInEvent.next(true);
				this.router.navigate(['/explore/products']).catch(() => {});
			}
		};
	}

	private _formInit() {
		this.form = new FormGroup();

		this.inputUsername = new FormField('username', 'Username', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThanOrEqualTo,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 3 }
				]
			},
			{ Name: ValidatorRuleEnum.Username, Options: [] },
			{ Name: ValidatorRuleEnum.UsernameUnique, Options: [] }
		]);

		this.inputEmail = new FormField('email', 'Email', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{ Name: ValidatorRuleEnum.Email, Options: [] },
			{ Name: ValidatorRuleEnum.EmailUnique, Options: [] },
		]);

		this.inputPassword = new FormField('password', 'Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.LengthGreaterThanOrEqualTo,
				Options: [
					{ Name: ValidatorOptionEnum.Min, Value: 6 }
				]
			},
			{
				Name: ValidatorRuleEnum.Confirm,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'passwordConfirm' }
				]
			}
		]);

		this.inputPasswordConfirm = new FormField('passwordConfirm', 'Confirm Password', null, [
			{ Name: ValidatorRuleEnum.Required, Options: [] },
			{
				Name: ValidatorRuleEnum.ConfirmFor,
				Options: [
					{ Name: ValidatorOptionEnum.CompareTo, Value: 'password' }
				]
			}
		]);

		this.form.SetFields([ this.inputUsername, this.inputEmail, this.inputPassword, this.inputPasswordConfirm ]);
		this.form.SetParent();
	}

}
