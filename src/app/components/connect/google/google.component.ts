import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { ConnectAPI } from '@app/models/api/connect/connect.classes';

@Component({
	selector: 'sprout-connect-google',
	encapsulation: ViewEncapsulation.Emulated,
	templateUrl: './google.component.html',
	styleUrls: [
		'./google.component.css'
	]
})
export class ConnectGoogleComponent implements OnInit {

	//

	private _code: string;

	constructor(
		public activatedRoute: ActivatedRoute,
		public accountModel: AccountModel,
		public connectAPI: ConnectAPI,
	) {
		this._code = '';
	}

	ngOnInit() {
		this._code = this.activatedRoute.snapshot.queryParams['code'];

		if (this.accountModel.IsLoggedIn()) {
			this._add();
		} else {
			this._authorize();
		}
	}

	// func : _

	private _add() {
		this.connectAPI.PostAddGoogle({
			Code: this._code
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
					this.accountModel.AccountConnectionsGoogle = true;

					window.close();
				}

				// TODO
				// error message
			},
			error => {
				// TODO
				// error message
			}
		);
	}

	private _authorize() {
		this.connectAPI.PostConnectGoogle({
			Code: this._code
		}).subscribe(
			(response: IResponse) => {
				let _result: IResult = response.Result;

				if (response.Success === true && _result.Code === 1) {
                    this.accountModel.SaveCookieAndLocal(_result.Data);

                    window.close();
				}

				// TODO
				// error message
			},
			error => {
				// TODO
				// error message
			}
		);
	}

}
