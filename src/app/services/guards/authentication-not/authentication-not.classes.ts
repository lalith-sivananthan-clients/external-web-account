import { Injectable } from '@angular/core';
import { Router, CanActivate, NavigationEnd } from '@angular/router';

import { AccountModel } from '@app/models/local/account/account.classes';

import { MessagerService } from '@app/services/core/messenger/messenger.classes';

@Injectable()
export class AuthenticationNotGuard implements CanActivate {

    // private _end = false;
    // private _currentRoute = '';

	constructor(
		private router: Router,
		private accountModel: AccountModel,
		private messagerService: MessagerService
	) {
        this.accountModel.Check();

        router.events.subscribe((data: any) => {
            // if (data instanceof NavigationEnd) {
            //     this._end = true;
            //     this._currentRoute = data.url;
            // }
        });
    }

	canActivate() {
        if (this.accountModel.IsLoggedIn() === false) {
            this.messagerService.CheckGlobal();

			return true;
        }

        this.router.navigate(['/create-account']).catch();
	}

}
