import { throwError as observableThrowError,  Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { AccountModel } from '@app/models/local/account/account.classes';

@Injectable()
export class ResponseHttpInterceptor implements HttpInterceptor {

	constructor(
		@Inject(Router) private router: Router,
		@Inject(AccountModel) private accountModel: AccountModel
	) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(request).pipe(
			tap((event: HttpEvent<any>) => {
				if (event instanceof HttpResponse) {
					return event;
				}
			}),
			catchError((error: HttpErrorResponse) => {
				if (error.status === 403) {
					this.accountModel.Logout();
					this.router.navigate(['/']).catch();
				}

				if (error.status === 550) {
				}

				console.log(error);
				return observableThrowError(error.statusText);
			})
		);
	}
}
