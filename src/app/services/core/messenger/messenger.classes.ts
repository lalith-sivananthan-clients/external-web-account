import { Injectable } from '@angular/core';

import { IMessage, IMessages } from '@app/services/core/language/langauge.interfaces';

import { AccountModel } from '@app/models/local/account/account.classes';

import { LanguageService } from '@app/services/core/language/language.classes';

import { MessengerGlobalEvent } from './messenger.variables';

@Injectable()
export class MessagerService {

	//

	private messages: IMessages = LanguageService.GetMessages('en');
	private message: IMessage = <IMessage>{};

	constructor(private accountModel: AccountModel) {}

	// func : public

	public CheckGlobal() {
		if (! this.accountModel.IsLoggedIn()) {
			return;
		}

		if (this.accountModel.AccountConfirmed === false) {
			MessengerGlobalEvent.next(this.messages.GlobalEmail.ConfirmCheck);
		} else {
			MessengerGlobalEvent.next(this.message);
		}
	}

}
