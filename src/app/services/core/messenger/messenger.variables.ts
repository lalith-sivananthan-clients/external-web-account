import { EventEmitter } from '@angular/core';

export let MessengerGlobalEvent: EventEmitter<any> = new EventEmitter();
