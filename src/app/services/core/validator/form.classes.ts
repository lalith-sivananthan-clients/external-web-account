import { IValidatorRule } from './validator.interfaces';
import { IResponse, IResult } from '@app/models/_shared/shared.interfaces';

import { ValidatorRuleEnum, ValidatorOptionEnum } from './validator.enums';

import { InjectorService } from '@app/services/core/injector/injector.classes';

import { AuthenticationAPI } from '@app/models/api/authentication/authentication.classes';

export class FormGroup {
	public IsValid: boolean;
	public Fields: FormField[];
	public AuthenticationAPI: AuthenticationAPI;

	constructor() {
		this.IsValid = false;

		this.AuthenticationAPI = InjectorService.Injector.get(AuthenticationAPI);
	}

	// func :

	public SetParent(): void {
		for (let field of this.Fields) {
			field.Parent = this;
		}
	}

	public SetFields(fields: FormField[]) {
		this.Fields = fields;
	}

	public GetField(name: string): FormField {
		for (let field of this.Fields) {
			if (field.Name === name) {
				return field;
			}
		}

		return null;
	}

	public Validate(): void {
		let _valid = true;

		for (let field of this.Fields) {
			if (field.IsValid === false) {
				_valid = false;
				break;
			}
		}

		this.IsValid = _valid;
	}
}

export class FormField {
	public Parent: FormGroup;
	public Name: string;
	public Label: string;
	public Value: string;
	public ValueOriginal: string;
	public Rules: IValidatorRule[];
	public IsValid: boolean;
	public IsEmpty: boolean;
	public IsDirty: boolean;
	public IsActive: boolean;
	public ErrorRule: ValidatorRuleEnum;
	public ErrorMessage: string;

	constructor(
		name: string,
		label: string,
		value: any,
		rules: IValidatorRule[]
	) {
		this.Name = name || '';
		this.Label = label || '';
		this.Value = value || '';
		this.ValueOriginal = this.Value;
		this.IsValid = false;
		this.IsEmpty = this.Value.length <= 0;
		this.IsDirty = false;
		this.IsActive = false;
		this.Rules = rules;
		this.ErrorRule = ValidatorRuleEnum.None;
		this.ErrorMessage = '';
	}

	async Validate(): Promise<any> {
		if (this.Value === null) {
			return false;
		}

		let _valid = true;
		let _rule: ValidatorRuleEnum;
		let _message = '';

		for (let field of this.Rules) {
			switch (field.Name) {
				case ValidatorRuleEnum.Required: {
					_valid = this.Value.length > 0;
					_rule = ValidatorRuleEnum.Required;
					_message = this.Label + ' is required';
					break;
				}
				case ValidatorRuleEnum.Email: {
					let _pattern = /^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

					_valid = _pattern.test(this.Value);
					_rule = ValidatorRuleEnum.Email;
					_message = this.Label + ' is invalid';
					break;
				}
				case ValidatorRuleEnum.EmailUnique: {
					let _response: IResponse = await this.Parent.AuthenticationAPI.PostEmailUnique({ Email: this.Value });
					let _result: IResult = _response.Result;

					_valid = _response.Success === true && _result.Code === 1 && _result.Data === true;
					_rule = ValidatorRuleEnum.EmailUnique;
					_message = this.Label + ' is already in use';
					break;
				}
				case ValidatorRuleEnum.Username: {
					let _pattern = /^[a-zA-Z0-9-_]+$/;

					_valid = _pattern.test(this.Value);
					_rule = ValidatorRuleEnum.Email;
					_message = this.Label + ' is invalid';

					break;
				}
				case ValidatorRuleEnum.UsernameUnique: {
					let _response: IResponse = await this.Parent.AuthenticationAPI.PostUsernameUnique({ Username: this.Value });
					let _result: IResult = _response.Result;

					_valid = _response.Success === true && _result.Code === 1 && _result.Data === true;
					_rule = ValidatorRuleEnum.EmailUnique;
					_message = this.Label + ' is already in use';

					break;
				}
				case ValidatorRuleEnum.LengthBetween: {
					let _min: number;
					let _max: number;

					for (let index in field.Options) {
						if (index) {
							if (field.Options[index].Name === ValidatorOptionEnum.Min) {
								_min = field.Options[index].Value;
							}

							if (field.Options[index].Name === ValidatorOptionEnum.Max) {
								_max = field.Options[index].Value;
							}
						}
					}

					_valid = this.Value.length >= _min && this.Value.length <= _max;
					_rule = ValidatorRuleEnum.LengthBetween;
					_message = this.Label + ' must be between ' + _min + ' and ' + _max;
					break;
				}
				case ValidatorRuleEnum.LengthGreaterThanOrEqualTo: {
					let _min: number;

					for (let index in field.Options) {
						if (index) {
							if (field.Options[index].Name === ValidatorOptionEnum.Min) {
								_min = field.Options[index].Value;
							}
						}
					}

					_valid = this.Value.length >= _min;
					_rule = ValidatorRuleEnum.LengthBetween;
					_message = this.Label + ' must be greater than ' + _min;
					break;
				}
				case ValidatorRuleEnum.Confirm: {
					let _confirm: FormField;

					for (let index in field.Options) {
						if (field.Options[index].Name === ValidatorOptionEnum.CompareTo) {
							_confirm = this.Parent.GetField(field.Options[index].Value);
							break;
						}
					}

					if (! _confirm.IsDirty) {
						break;
					}

					this.Parent.GetField(_confirm.Name).Validate();
					break;
				}
				case ValidatorRuleEnum.ConfirmFor: {
					let _confirm: FormField;

					for (let index in field.Options) {
						if (field.Options[index].Name === ValidatorOptionEnum.CompareTo) {
							_confirm = this.Parent.GetField(field.Options[index].Value);
							break;
						}
					}

					_valid = this.Value === _confirm.Value;
					_rule = ValidatorRuleEnum.LengthBetween;
					_message = _confirm.Label + ' does not match ' + this.Label;
					break;
				}
			}

			if (_valid === false) {
				break;
			}
		}

		this.IsValid = _valid;
		this.ErrorRule = _rule;
		this.ErrorMessage = _message;
		this.Parent.Validate();
	}
}
