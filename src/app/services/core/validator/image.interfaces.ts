export interface IValidatorImage {
	Width: number;
	Height: number;
}

export interface IValidatorImageRules {
	Ratio: number;
	MinWidth: number;
	MaxWidth: number;
	MinHeight: number;
	MaxHeight: number;
	Width: number;
	Height: number;
}
