import { IValidatorImage, IValidatorImageRules } from './image.interfaces';

export function ImageFindDimension(file: File): Promise<IValidatorImage> {
	return new Promise(function(resolve, reject) {
		let image = new Image();
		image.src = URL.createObjectURL(file);

		image.onload = () => {
			URL.revokeObjectURL(image.src);

			if (image.complete) {
				resolve({
					Width: image.naturalWidth,
					Height: image.naturalHeight,
				});
			} else {
				reject(Error('you derped it'));
			}
		};
	});
}

export function ImageValidateDimension(rules: IValidatorImageRules): boolean {
	return ! (
		rules.Width <= rules.MinWidth && rules.MinWidth !== 0 ||
		rules.Width >= rules.MaxWidth && rules.MaxWidth !== 0 ||
		rules.Height <= rules.MinHeight && rules.MinHeight !== 0 ||
		rules.Height >= rules.MaxHeight && rules.MaxHeight !== 0
	);
}
