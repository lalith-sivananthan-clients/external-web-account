export enum MessageTypeEnum {
	None    = <any>'',
	Info    = <any>'info',
	Warning = <any>'warning',
	Success = <any>'success',
	Error   = <any>'error'
}
