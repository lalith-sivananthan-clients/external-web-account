import { IMessages } from './langauge.interfaces';

import { GlobalBasicMessages, GlobalEmailMessages } from './en/messages/global.variables';
import { AccountMessages } from './en/messages/account.variables';

export const EnMessages: IMessages = {
	GlobalBasic: GlobalBasicMessages,
	GlobalEmail: GlobalEmailMessages,
	Account: AccountMessages
};
